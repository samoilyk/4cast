//
//  ForecastModel.swift
//  SomeApp
//
//  Created by Ievgen on 10/17/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import Foundation

// Weather data: Current condition, 5 day forecast or long term
enum ForecastType
{
    case today
    case fiveDaysEach3h
    case longterm
}

// Days of week enum
enum WeekDay: Int
{
    case Sunday = 1
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
}

// Alert messages
enum AlertMessage: String
{
    case noInternetConnection = "No Internet connection. Pull to refresh after it will be back"
    case noLocationService = "It looks like there is some problem with location services. Make sure they are allowed for this app"
    case someOtherProblem = "Some problem occurred while loading data, please try again later"
}

// Weather condition
struct Weather
{
    var temperature = "-"
    var windSpeed = ""
    var rain = ""
    var iconName = "placeholder"
    var weekDay: WeekDay?
    var dayTime = ""
}
