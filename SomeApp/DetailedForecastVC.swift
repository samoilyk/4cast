//
//  DetailedForecastVC.swift
//  SomeApp
//
//  Created by Ievgen on 10/25/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit

class DetailedForecastVC: UIViewController, UITableViewDataSource, UITableViewDelegate, LocationServicesDelegate
{
    // Outlets
    @IBOutlet weak var detailedForecastTableView: UITableView!
    @IBOutlet weak var navigationView: NavigationView!

    // Refresh control for table view
    private var refreshControl = UIRefreshControl()
    
    // Location services
    private var locationService = LocationServices()

    // Table view data source
    private var forecastArray = [[Weather]]() {
        didSet {
            detailedForecastTableView.reloadData()
        }
    }
    
    let daysCount = 5


    
    // MARK: - viewDidLoad
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Hide empty cells
        detailedForecastTableView.tableFooterView = UIView(frame: .zero)

        // Add refreshControl to the table view
        refreshControl.addTarget(self, action: #selector(DetailedForecastVC.requestNewLocation), for: .valueChanged)
        detailedForecastTableView.addSubview(refreshControl)
        
        // Location services
        locationService.delegate = self
        requestNewLocation()
   }
    
    
    
    // Status bar style
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    
    // MARK: - Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return forecastArray.count // Five days forecast
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return forecastArray[section].count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Detailed cell") as! DetailedTableViewCell
        cell.weatherForeCast = forecastArray[indexPath.section][indexPath.item]
        
        return cell
    }
    
    
    
    // Section headers
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 35))
        view.backgroundColor = UIColor.white
        
        let label = UILabel(frame: CGRect(x: 10, y: 5, width: view.frame.width, height: view.frame.height - 5*2))
        label.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightLight)
        if let weekDay = forecastArray[section].first?.weekDay {
            label.text = String(describing: weekDay)
        }
        view.addSubview(label)
        
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 35
    }
    
    
    
    
    // MARK: - Get data from server(LocationServices delegate methods)
    
    func getWeatherData(for coordinates: (Double,Double))
    {
        // Data for current condition
        WeatherData.sharedInstance.getWeatherConditions(for: .fiveDaysEach3h, at: coordinates) { [weak weakSelf = self] (response) in
            
            if let forecasts = response,
                let mutatedArray = weakSelf?.mutate(forecasts) {
                weakSelf?.forecastArray = mutatedArray
            }
                
            // If it fails loading data and response is nil
            else {
                
                // Check for internet connection and show alert
                let isConnected = Reachability.isConnectedToNetwork()
                isConnected ? weakSelf?.showAlertController(with: .someOtherProblem) :
                    weakSelf?.showAlertController(with: .noInternetConnection)
            }
            
            // Stop activity indicator animation
            weakSelf?.navigationView.activityIndicator.stopAnimating()
            
            // Stop refreshing
            weakSelf?.refreshControl.endRefreshing()
        }
    }
    
    
    // MARK: - Alert controller
    
    func showAlertController(with message: AlertMessage)
    {
        // Stop animation
        if message == .noLocationService {
            refreshControl.endRefreshing()
        }
        
        // Title
        let alert = UIAlertController(title: message.rawValue, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        // Ok action to dismiss alert controller
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in
            
            alert.dismiss(animated: true, completion: nil)
            
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func setCityName(with name: String) {} // Action is not needed
    
    
    
    // MARK: - Request new location
    
    func requestNewLocation()
    {
        // Request new location
        locationService.locationManager.requestLocation()
        
        // Show activity indicator
        navigationView.startActivityIndicatorAnimation()
    }
    
    
    func mutate(_ array: [Weather]) -> [[Weather]]
    {
        var oldArray = array
        var arrayToReturn = [[Weather]]()

        for _ in 0..<daysCount
        {
            let arrayToAppend = oldArray.filter { $0.weekDay == oldArray[0].weekDay }
            arrayToReturn.append(arrayToAppend)
            oldArray = oldArray.filter { $0.weekDay != oldArray[0].weekDay }
        }
        return arrayToReturn
    }
}
