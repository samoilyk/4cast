//
//  DetailedTableViewCell.swift
//  SomeApp
//
//  Created by Ievgen on 10/27/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit

class DetailedTableViewCell: UITableViewCell
{
    @IBOutlet weak var temperatureLabel: Label!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    
    var weatherForeCast = Weather() {
        didSet
        {
            temperatureLabel.text = weatherForeCast.temperature + "°"
            weatherImageView.image = UIImage(named: weatherForeCast.iconName)
            rainLabel.text = weatherForeCast.rain + " mm"
            windSpeedLabel.text = weatherForeCast.windSpeed + " m/s"
            timeLabel.text = weatherForeCast.dayTime
        }
    }
}
