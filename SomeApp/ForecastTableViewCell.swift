//
//  ForecastTableViewCell.swift
//  SomeApp
//
//  Created by Ievgen on 10/18/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell
{
    @IBOutlet weak var temperatureLabel: Label!
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    
    var weatherForeCast = Weather()
    {
        didSet
        {
            temperatureLabel.text = weatherForeCast.temperature + "°"
            if let weekday = weatherForeCast.weekDay {
                weekdayLabel.text = String(describing: weekday)
            }
            weatherImageView.image = UIImage(named: weatherForeCast.iconName)
        }
    }

}
