//
//  WeatherData.swift
//  SomeApp
//
//  Created by Ievgen on 10/17/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON // Easy and safe way to work with json


class WeatherData
{
    // Singleton constant
    static let sharedInstance = WeatherData()
    private init() {}
    
    // Base api url
    private let API_URL = "http://api.openweathermap.org/data/2.5/"
    
    // App id to get forecasts
    private let id = "302e72cba227b7c55d54896b0b437073"
    
    // Number of days to show forecast for
    private let numberOfDays = 7
    
    
    
    // Get current weather condition
    func getWeatherConditions(for forecastType: ForecastType, at coordinates: (lat: Double, lon: Double), completion: @escaping (_ response: [Weather]?) -> ())
    {
        let apiUrl = createUrl(for: forecastType, at: coordinates)
        
        Alamofire.request(apiUrl).validate().responseJSON { [weak weakSelf = self] response in
            
            switch response.result {
            
            // Response is success
            case .success(let value):
                
                let json = JSON(value)
                let weatherArray = weakSelf?.parse(json, for: forecastType)
                
                completion(weatherArray)
                    
            // Error
            case .failure(let error):
                
                _ = Lgg(error)
                
                // Pass nil if response.result is not success
                completion(nil)
            }
        }
    }
    
    
    
    // Create url for current weather condition or forecast
    private func createUrl(for forecastType: ForecastType, at coordinates: (lat: Double, lon: Double)) -> String
    {
        switch forecastType {
            
        // Current condition
        case .today:
            let url = API_URL + "weather?lat=\(coordinates.lat)&lon=\(coordinates.lon)&units=metric&APPID=\(id)"
            return url
        
        // For five days
        case .fiveDaysEach3h:
            let url = API_URL + "forecast?lat=\(coordinates.lat)&lon=\(coordinates.lon)&units=metric&APPID=\(id)"
            return url
        
        // Long Term
        case .longterm:
            let url = API_URL + "forecast/daily?lat=\(coordinates.lat)&lon=\(coordinates.lon)&cnt=\(numberOfDays)&units=metric&APPID=\(id)"
            return url
        }
    }
    
    
    
    // Getting Weekday from timestamp
    private func getWeekDay(from timeInterval: TimeInterval?) ->  WeekDay?
    {
        guard let timeStamp = timeInterval else { return nil}
        let todayDate = Date(timeIntervalSince1970: timeStamp)
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        
        return WeekDay(rawValue: weekDay)
    }
    
    
    
    // Parser
    private func parse(_ json: JSON, for dataTypeNeeded: ForecastType) -> [Weather]
    {
        // Set parameters for today
        switch dataTypeNeeded {

        case .today:
            
            var weather = Weather()
            var weatherArray = [Weather]()
            
            // Temperature
            if let temperature = json["main"]["temp"].double {
                weather.temperature = String(describing: lround(temperature))
            }
            
            // Weather icon
            if let iconName = json["weather"][0]["icon"].string {
                weather.iconName = iconName
            }
            
            // Wind speed
            if let windSpeed = json["wind"]["speed"].double {
                weather.windSpeed = String(describing: Double(round(10*windSpeed)/10))
            } else {
                weather.windSpeed = "0"
            }
            
            // Rain or snow
            if let rain = json["rain"]["3h"].double {
                weather.rain = String(describing: Double(round(10*rain)/10))
            } else if let snow = json["snow"]["3h"].double {
                weather.rain = String(describing: Double(round(10*snow)/10))
            } else {
                weather.rain = "0"
            }
            
            weatherArray.append(weather)
            return weatherArray
            
        // For five days
        case .fiveDaysEach3h:
            
            var weather = Weather()
            var weatherArray = [Weather]()
            
            let forecastArray = json["list"]
            for (_, subJson): (String, JSON) in forecastArray
            {
                if let temperature = subJson["main"]["temp"].double {
                    weather.temperature = String(describing: lround(temperature))
                }
                if let iconName = subJson["weather"][0]["icon"].string  {
                    weather.iconName = iconName
                }
                
                if let windSpeed = subJson["wind"]["speed"].double {
                    weather.windSpeed = String(describing: Double(round(10*windSpeed)/10))
                } else {
                    weather.windSpeed = "0"
                }
                
                if let rain = subJson["rain"]["3h"].double {
                    weather.rain = String(describing: Double(round(10*rain)/10))
                } else if let snow = subJson["snow"]["3h"].double {
                    weather.rain = String(describing: Double(round(10*snow)/10))
                } else {
                    weather.rain = "0"
                }
            
                // Set weekday
                let timeInterval = subJson["dt"].double
                weather.weekDay = getWeekDay(from: timeInterval)
                
                // Day time
                if let dateString = subJson["dt_txt"].string
                {
                    // Get time from string
                    let index = dateString.index(dateString.endIndex, offsetBy: -8)
                    let dayTimeLong = dateString.substring(from: index)
                    // Shortened vesriosn
                    let index1 = dayTimeLong.index(dayTimeLong.endIndex, offsetBy: -3)
                    let dayTime = dayTimeLong.substring(to: index1)
                    weather.dayTime = dayTime
                }
                
                weatherArray.append(weather)
            }
            return weatherArray
            
        case .longterm:

            var weather = Weather()
            var weatherArray = [Weather]()
            
            let forecastArray = json["list"]
            for (_, subJson): (String, JSON) in forecastArray
            {
                if let temperature = subJson["temp"]["day"].double {
                    weather.temperature = String(describing: lround(temperature))
                }
                if let iconName = subJson["weather"][0]["icon"].string  {
                    weather.iconName = iconName
                }
                
                // Set weekday
                let timeInterval = subJson["dt"].double
                weather.weekDay = getWeekDay(from: timeInterval)
                
                weatherArray.append(weather)
            }
            return weatherArray
        }
    }
}
