//
//  NavigationView.swift
//  SomeApp
//
//  Created by Ievgen on 10/19/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit


class NavigationView: UIView
{
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Animations
    
    func startActivityIndicatorAnimation()
    {
        activityIndicator.startAnimating()
        
        UIView.transition(with: activityIndicator, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.activityIndicator.isHidden = false
            
            }, completion: nil)
    }
    
    
    
    func stopActivityIndicatorAnimation()
    {
        UIView.transition(with: activityIndicator, duration: 0.5, options: .transitionCrossDissolve, animations: {
            
            self.activityIndicator.isHidden = true
            
        }) { (true) in
            self.activityIndicator.stopAnimating()
        }
    }
    
}
