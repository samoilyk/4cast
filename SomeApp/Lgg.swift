//
//  Lgg.swift
//  SomeApp
//
//  Created by Ievgen on 10/17/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import Foundation

class Lgg
{
    init(_ object: Any?)
    {
        #if DEBUG
        if let objectToPrint = object {
            print(objectToPrint)
        }
        #endif
    }
}
