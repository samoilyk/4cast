//
//  MainVC.swift
//  SomeApp
//
//  Created by Ievgen on 10/17/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, LocationServicesDelegate
{
    // Outlets
    @IBOutlet weak var forecastTableView: UITableView!
    @IBOutlet weak var currentConditionView: CurrentConditionView!
    @IBOutlet weak var navigationView: NavigationView!

    
    // Refresh control for table view
    private var refreshControl = UIRefreshControl()
    // Location service object
    private var locationService = LocationServices()
    
    private var currentWeatherCondition = Weather() {
        didSet {
            currentConditionView.currentWeatherCondition = currentWeatherCondition
            
        }
    }
    
    // Array with forecasts - data source for table view
    private var forecastArray = [Weather]() {
        didSet {
            // Remove forecast for today
            forecastArray.removeFirst()
            // Reload table view
            forecastTableView.reloadData()
        }
    }
    
    
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Hide empty cells
        forecastTableView.tableFooterView = UIView(frame: .zero)

        // Add refreshControl to the table view
        refreshControl.addTarget(self, action: #selector(MainVC.requestNewLocation), for: .valueChanged)
        forecastTableView.addSubview(refreshControl)
        
        // Location services
        locationService.delegate = self
        requestNewLocation()
    }
    
    
    
    // Status bar style
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    
    // MARK: - Table view data source & delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return forecastArray.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Forecast cell") as? ForecastTableViewCell else { return ForecastTableViewCell() }
        cell.weatherForeCast = forecastArray[indexPath.row]
        
        return cell
    }
    
    
    
    //MARK: - Getting data from server
    
    func getWeatherData(for coordinates: (Double,Double))
    {
        
        // Data for current condition
        WeatherData.sharedInstance.getWeatherConditions(for: .today, at: coordinates) { [weak weakSelf = self] (response) in
            
            if let weatherCondition = response?.first
            {
                weakSelf?.currentWeatherCondition = weatherCondition
            }
                
            // If it fails loading data and response is nil
            else {
                
                // Check for internet connection and show alert
                let isConnected = Reachability.isConnectedToNetwork()
                isConnected ? weakSelf?.showAlertController(with: .someOtherProblem) :
                                weakSelf?.showAlertController(with: .noInternetConnection)
            }
            
            // Stop activity indicator animation
            weakSelf?.navigationView.stopActivityIndicatorAnimation()
        }
        
        // Data for forecast
        WeatherData.sharedInstance.getWeatherConditions(for: .longterm, at: coordinates) { [weak weakSelf = self] (response) in
            
            if let forecasts = response
            {
                weakSelf?.forecastArray = forecasts
            }
            
            // Stop refreshing
            weakSelf?.refreshControl.endRefreshing()
        }
        
    }
    
    
    
    // Location services delegate sets city name
    func setCityName(with name: String)
    {
        currentConditionView.cityNameLabel.text = name
    }
    
    
    
    // MARK: - Request new location
    
    func requestNewLocation()
    {
        // Request new location
        locationService.locationManager.requestLocation()
        
        // Activity indicator
        navigationView.startActivityIndicatorAnimation()
    }
    
    
    
    // MARK: - Alert controller
    
    func showAlertController(with message: AlertMessage)
    {
        // Stop animation
        if message == .noLocationService {
            navigationView.stopActivityIndicatorAnimation()
            refreshControl.endRefreshing()
        }
        
        // Title
        let alert = UIAlertController(title: message.rawValue, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        // Ok action to dismiss alert controller
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in

            alert.dismiss(animated: true, completion: nil)
            
        }))
        
        present(alert, animated: true, completion: nil)
    }
}
