//
//  CurrentConditionView.swift
//  SomeApp
//
//  Created by Ievgen on 10/19/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit

class CurrentConditionView: UIView
{
    // Outlets
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: Label!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    
    
    var currentWeatherCondition = Weather() {
        didSet {
            temperatureLabel.text = currentWeatherCondition.temperature + "°"
            weatherImageView.image = UIImage(named: currentWeatherCondition.iconName)
            rainLabel.text = currentWeatherCondition.rain + " mm"
            windSpeedLabel.text = currentWeatherCondition.windSpeed + " m/s"
        }
    }
}


// UILabel subclass
class Label: UILabel
{
    override var text: String? {
        didSet {
            text!.contains("-") ? (self.textColor = UIColor(red: 25/255, green: 211/255, blue: 255/255, alpha: 1.0)) :
                                    (self.textColor = UIColor.red)
        }
    }
}
