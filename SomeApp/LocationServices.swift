//
//  LocationServices.swift
//  SomeApp
//
//  Created by Ievgen on 10/25/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationServicesDelegate: class
{
    func getWeatherData(for coordinates: (Double,Double))
    func showAlertController(with message: AlertMessage)
    func setCityName(with name: String)
}

class LocationServices: NSObject, CLLocationManagerDelegate
{
    weak var delegate: LocationServicesDelegate?
    
    var locationManager = CLLocationManager()
    
    
    // Init
    override init()
    {
        super.init()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
    }
    
    
    
    // MARK: - Location manger delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let location = locations.last,
            let del = delegate {
            
            // Coordinates tuple
            let coordinates = (location.coordinate.latitude, location.coordinate.longitude)
            
            // Get data from server
            del.getWeatherData(for: coordinates)
            
            // Set city name fore current location
            getCityName(for: location)
            
            // Stop updating
            locationManager.stopUpdatingLocation()
        }
        
        else {
            _ = Lgg("error while getting loaction")
            return
        }
    }
    
    
    
    // In case it failed to get a location
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {      
        // Stop updating
        locationManager.stopUpdatingLocation()
        
        // Show alert controller
        if let del = delegate {
            del.showAlertController(with: .noLocationService)
        }

        _ = Lgg(error)
    }
    
    
    // City name
    private func getCityName(for location: CLLocation)
    {
        CLGeocoder().reverseGeocodeLocation(location) { [weak weakSelf = self] (placemarks, error) in
            if (error == nil)
            {
                guard let placemark = placemarks?.first else { return }
                let pm = placemark as CLPlacemark
                
                if let del = weakSelf?.delegate,
                    let name = pm.locality {
                    del.setCityName(with: name)
                }
            }
        }
    }
}
